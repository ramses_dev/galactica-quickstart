#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of the 'astrophysix' Python package.
#
# Copyright © Commissariat a l'Energie Atomique et aux Energies Alternatives (CEA)
#
#  FREE SOFTWARE LICENCING
#  -----------------------
# This software is governed by the CeCILL license under French law and abiding by the rules of distribution of free
# software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by
# CEA, CNRS and INRIA at the following URL: "http://www.cecill.info". As a counterpart to the access to the source code
# and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty
# and the software's author, the holder of the economic rights, and the successive licensors have only limited
# liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying
# and/or developing or reproducing the software by the user in light of its specific status of free software, that may
# mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
# experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
# software's suitability as regards their requirements in conditions enabling the security of their systems and/or data
# to be ensured and, more generally, to use and operate it in the same conditions as regards security. The fact that
# you are presently reading this means that you have had knowledge of the CeCILL license and that you accept its terms.
#
#
# COMMERCIAL SOFTWARE LICENCING
# -----------------------------
# You can obtain this software from CEA under other licencing terms for commercial purposes. For this you will need to
# negotiate a specific contract with a legal representative of CEA.
#


# We keep the import section
from __future__ import print_function, unicode_literals

import os
from time import time

import pandas as pd
from astrophysix import units as U
from astrophysix.simdm import Project, ProjectCategory, SimulationStudy
from astrophysix.simdm.datafiles import Datafile, PlotInfo, PlotType
from astrophysix.simdm.experiment import (
    AppliedAlgorithm,
    ParameterSetting,
    ParameterVisibility,
    ResolvedPhysicalProcess,
    Simulation,
)
from astrophysix.simdm.protocol import (
    Algorithm,
    AlgoType,
    InputParameter,
    PhysicalProcess,
    Physics,
    SimulationCode,
)
from astrophysix.simdm.results import GenericResult, Snapshot
from astrophysix.utils.file import FileType

# ----------------------------------------------- Project creation --------------------------------------------------- #
# Available project categories are :
# - ProjectCategory.SolarMHD
# - ProjectCategory.PlanetaryAtmospheres
# - ProjectCategory.StarPlanetInteractions
# - ProjectCategory.StarFormation
# - ProjectCategory.Supernovae
# - ProjectCategory.GalaxyFormation
# - ProjectCategory.GalaxyMergers
# - ProjectCategory.Cosmology

# We adapt the project description.
# It's easier to edit it online. But be sure to have it saved somewhere, because 
# a new import will override the online version
proj = Project(
    category=ProjectCategory.StarFormation,
    project_title="Ismfeed2",
    alias="ISMFEED2",
    short_description="Impact of feedback and turbulence on star formation",
    general_description="""Impact of feedback and turbulence on star formation. The simulation presented here are described in Brucy et al. 2020, ApJL, L38""",
    data_description="The data available in this project...",
    directory_path="~nbrucy/simus/ismfeed",
)
print(proj)
# -------------------------------------------------------------------------------------------------------------------- #


# --------------------------------------- Simulation code definition ------------------------------------------------- #
# Describe the simulation code. It's a bit long but you can skip some part (and maybe come back later)
# Also you only need to do it once

ramses = SimulationCode(
    name="Ramses 3 (MHD)",
    code_name="Ramses",
    code_version="3.10.1",
    alias="RAMSES_3",
    url="https://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html",
    description="Ramses MHD code",
)
# => Add algorithms : available algorithm types are :
# - AlgoType.AdaptiveMeshRefinement
# - AlgoType.VoronoiMovingMesh
# - AlgoType.SmoothParticleHydrodynamics
# - AlgoType.Godunov
# - AlgoType.PoissonMultigrid
# - AlgoType.PoissonConjugateGradient
# - AlgoType.ParticleMesh
# - AlgoType.FriendOfFriend
# - AlgoType.HLLCRiemann
# - AlgoType.RayTracer
# amr = ramses.algorithms.add(Algorithm(algo_type=AlgoType.AdaptiveMeshRefinement, description="AMR"))
ramses.algorithms.add(
    Algorithm(algo_type=AlgoType.Godunov, description="Godunov scheme")
)
ramses.algorithms.add(
    Algorithm(algo_type=AlgoType.HLLCRiemann, description="HLLC Riemann solver")
)
ramses.algorithms.add(
    Algorithm(
        algo_type=AlgoType.PoissonMultigrid, description="Multigrid Poisson solver"
    )
)
ramses.algorithms.add(
    Algorithm(algo_type=AlgoType.ParticleMesh, description="PM solver")
)

# => Add input parameters
ramses.input_parameters.add(
    InputParameter(
        key="levelmin",
        name="Lmin",
        description="min. level of AMR refinement",
    )
)
ramses.input_parameters.add(
    InputParameter(
        key="levelmax",
        name="Lmax",
        description="max. level of AMR refinement",
    )
)

ramses.input_parameters.add(
    InputParameter(
        key="turb_rms", name="f_rms", description="Amplitude of the driving"
    )
)
ramses.input_parameters.add(
    InputParameter(
        key="dens0", name="n_0", description="Midplane density in cm**-3"
    )
)
ramses.input_parameters.add(
    InputParameter(
        key="bx_bound",
        name="bx_bound",
        description="imposed magnetic field at the x boundary (1 implies that the magnetic pressure is equal to thermal pressure in WNM, which corresponds to about 5muG)",
    )
)


# => Add physical processes : available physics are :
# - Physics.SelfGravity
# - Physics.Hydrodynamics
# - Physics.MHD
# - Physics.StarFormation
# - Physics.SupernovaeFeedback
# - Physics.AGNFeedback
# - Physics.MolecularCooling
ramses.physical_processes.add(
    PhysicalProcess(
        physics=Physics.StarFormation,
        description="Star Formation is triggered when density overpass",
    )
)
ramses.physical_processes.add(
    PhysicalProcess(
        physics=Physics.MHD, description="Magneto-hydrodynamical equations are solved"
    )
)
ramses.physical_processes.add(
    PhysicalProcess(physics=Physics.SelfGravity, description="Self-Gravity is applied.")
)
ramses.physical_processes.add(
    PhysicalProcess(physics=Physics.SupernovaeFeedback, description="SN feedback")
)
# -------------------------------------------------------------------------------------------------------------------- #


# -------------------------------------------- Simulation setup ------------------------------------------------------ #
# Setup you simulation. Galactica is quite picky with aliases (IN CAPITAL LETTERS and the format of the execution time)
simu = Simulation(
    simu_code=ramses,
    name="n1.5",
    alias="DENS_1_5",
    description="Simulation without turbulence",
    directory_path="~nbrucy/simus/turb/",
    execution_time="2020-03-01 18:45:30"

)
proj.simulations.add(simu)

# Add applied algorithms implementation details. Warning : corresponding algorithms must have been added in the 
# simulation code.

# I'll skipped that part for now

# simu.applied_algorithms.add(AppliedAlgorithm(algorithm=amr, details="My AMR implementation [Teyssier 2002]"))
# simu.applied_algorithms.add(AppliedAlgorithm(algorithm=ramses.algorithms[AlgoType.HLLCRiemann.name],
#                                             details="My Riemann solver implementation [Teyssier 2002]"))

# Add parameter setting. Warning : corresponding input parameter must have been added in the  simulation code.
# Available parameter visibility options are :
# - ParameterVisibility.NOT_DISPLAYED
# - ParameterVisibility.ADVANCED_DISPLAY
# - ParameterVisibility.BASIC_DISPLAY
simu.parameter_settings.add(
    ParameterSetting(
        input_param=ramses.input_parameters["Lmin"],
        value=8,
        visibility=ParameterVisibility.BASIC_DISPLAY,
    )
)
simu.parameter_settings.add(
    ParameterSetting(
        input_param=ramses.input_parameters["Lmax"],
        value=8,
        visibility=ParameterVisibility.BASIC_DISPLAY,
    )
)
simu.parameter_settings.add(
    ParameterSetting(
        input_param=ramses.input_parameters["f_rms"],
        value=0,
        visibility=ParameterVisibility.BASIC_DISPLAY,
    )
)
simu.parameter_settings.add(
    ParameterSetting(
        input_param=ramses.input_parameters["n_0"],
        value=1.5,
        visibility=ParameterVisibility.BASIC_DISPLAY,
    )
)
# Add resolved physical process implementation details. Warning : corresponding physical process must have been added to
# the  simulation code
simu.resolved_physics.add(
    ResolvedPhysicalProcess(
        physics=ramses.physical_processes[Physics.StarFormation.name],
        details="Star formation specific implementation",
    )
)
simu.resolved_physics.add(
    ResolvedPhysicalProcess(
        physics=ramses.physical_processes[Physics.SelfGravity.name], 
        details="self-gravity specific implementation"
    )
)
# -------------------------------------------------------------------------------------------------------------------- #


# -------------------------------------- Simulation generic result and snapshots ------------------------------------- #

# Generic result

# These are result that apply to all the simulation. I skip that part for now.

# gres = GenericResult(
#     name="Key result 1 !",
#     description="My description",
#     directory_path="/my/path/to/result",
# )
# simu.generic_results.add(gres)

# Then we add simulation snapshots.
# We can use for loop if you wan to describe several of them
# Simulation snapshots
time0 = 24.1
snapshots = {}
for i in range(10, 90, 10):
    sn = simu.snapshots.add(
        Snapshot(
            name=f"Output {i}",
            description=f"Snapshot {i}",
            time=(time0 + i*3, U.Myr),
            physical_size=(1.0, U.kpc),
            directory_path=f"output_000{i}",
            data_reference=f"OUTPUT_000{i}",
        )
    )
    # Store it to add datafiles later
    snapshots[i] = sn
# -------------------------------------------------------------------------------------------------------------------- #


# ---------------------------------------------------- Result datafiles ---------------------------------------------- #
# Datafile creation

# I have several kinds of plots. I use a dictionary to avoid reapeating their name and description.
plots = {
    "coldens_z" : ("Column density z", "Column density face-on"),
    "slice_rho_z" : ("Density slice z", "Density slice face-on"),
    "slice_T_z" : ("Temperature slice z", "Temperature slice face-on")
}

for i in range(10, 90, 10):
    for key in plots:
        df = snapshots[i].datafiles.add(
            Datafile(
                name=plots[key][0],
                description=plots[key][1],
            )
        )
    # Add attached files to a datafile (1 per file type). 
        df[FileType.JPEG_FILE] = f"n1.5/jpeg/{key}_live_n1.5_{i:05}.jpeg"


# I also have a single PDF result for the snapshot 10. 
# I can add it, as well as a CSV file that contains  the data used to make it.
pdf_df = snapshots[10].datafiles.add(
            Datafile(
                name="Density PDF",
                description="Mass weighted PDF of the density",
            )
        )

# Add attached files to a datafile (1 per file type). 
pdf_df[FileType.JPEG_FILE] = "n1.5//jpeg/rho_pdf_live_n1.5_00010.jpeg"
pdf_df[FileType.CSV_FILE] = "n1.5//csv/rho_pdf_live_n1.5_00010.csv"

data = pd.read_csv("n1.5//csv/rho_pdf_live_n1.5_00010.csv")

# Datafile plot information
# Adding these informations enables Galactica to do nice interactive plots!
pdf_df.plot_info = PlotInfo(
    plot_type=PlotType.LINE_PLOT,
    xaxis_values=data.logrho.values,
    yaxis_values=data.PDF.values,
    xaxis_log_scale=False,
    yaxis_log_scale=True,
    xlabel="$\log \rho$",
    ylabel="PDF",
    xaxis_unit=U.H_cc,
    plot_title="Density PDF",
    yaxis_unit=U.none,
)
# -------------------------------------------------------------------------------------------------------------------- #


# Save study in HDF5 file

# Once everything is finished we can expotr the project to an hdf5 file that can be imported on Galactica
# Note the galactica_checks option. It will checks that all the names, alias, etc., comply with the Galactica requirements.
study = SimulationStudy(project=proj)
study.save_HDF5("./ismfeed2_study.h5", galactica_checks=True)
