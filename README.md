# Galactica-quickstart

Quickstart guide to Galactica and astrophysix package.
This repository contains the basic script than need to be adapted to upload your own simulation on Galactica.
It also contains a small example on a real simulation.

Authors: Damien Chapon, guide adapted by Noé Brucy.

Galactica database: http://www.galactica-simulations.eu/db/

See https://astrophysix.readthedocs.io/en/stable/usage/quickstart.html

## Prerequirements

### Install the astrophysix package.
```pip install astrophysix```

### Request an account on Galactica: 

To create a new user account, please send an e-mail to admin@galactica-simulations.eu with the following information:

   - your name,
   - the name of your institute/organization (if any),
   - the reasons why you want to access the data available on Galactica.

A Galactica administrator wil send you your authentication information by e-mail shortly.

### Read the docs

It's there:  https://astrophysix.readthedocs.io/en/stable

## Get started

- The quickstart guide is the file `quickstart.py`.
- The example simulation is in the `n1.5` folder and the upload script is `example.py`. It covers basic description of the simulations and show
how the creation of upload file for Galactica can be automated.
- It creates a hdf5 file that can be uploaded in Galactica 
